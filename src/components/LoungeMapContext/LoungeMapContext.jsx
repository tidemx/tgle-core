import React, {createContext, useState} from 'react';

export const LoungeMapContext = createContext(5);

const LoungeMapContextProvider= ({children}) => {
    const [extraInfo, setExtraInfo] = useState({});
    // perhaps add validation logic to setExtraInfo...
    const value = {extraInfo, setExtraInfo};
    return (
        <LoungeMapContext.Provider value={value}>
            {children}
        </LoungeMapContext.Provider>
    );
};

export default LoungeMapContextProvider;
