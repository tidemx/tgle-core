import React, {useState, useCallback, useRef, useLayoutEffect, useMemo} from 'react';
import './LoungeMap.scss';
import { Stage, Layer, Rect, Text} from 'react-konva';
import ResizeSensor from "css-element-queries/src/ResizeSensor";
import KCPathButton from "../components/KCPathButton";
import kcPaths from "../../assets/kcPaths";
import KCMapItem from "../components/KCMapItem";
import {computeViewPortRatio, guestFullName} from "../../utils/loungeMapUtils";
import {getRelativePointerPosition, maxDecimals} from "../../utils/konvaUtils";
import classNames from 'classnames';
import {isSeatInVisit} from "../../utils/loungeMapItemUtils";
import _ from 'lodash';

const noop = ()=>{};
const LoungeMap = ({
    className,
    editable,
    hideText,
    loungeMap,
    onItemChange,
    onItemSelect=noop,
    onStageClick=noop,
    onViewPortChange,
    reservations,
    selectedItem,
    selectedVisit,
    viewPort,

    mapType='default', // to determine which set of components to use and avoid having all-purpose map item components

    extraInfo
})=>{

    const containerRef = useRef(null);
    const stageRef = useRef(null);
    const interactiveLayerRef = useRef(null);
    const [size, setSize]=useState({width: 500, height: 500});


    // -------- Set canvas size to the size of the div, and listen for changes to keep in sync
    useLayoutEffect(()=>{
        if(!containerRef.current)
            return;
        const size={ width: containerRef.current.offsetWidth, height: containerRef.current.offsetHeight };
        setSize(size);
        const callback = ()=>
            setSize({ width: containerRef.current.offsetWidth, height: containerRef.current.offsetHeight });
        const sensor = new ResizeSensor(containerRef.current, callback);
        return () => sensor.detach(callback);
    },[]);

    // ----------  Dragging for navigation
    const {width:lmWidth, height:lmHeight} = loungeMap.config||{width: 1000, height: 1000};
    const {viewRatio, offset} = useMemo(()=>{
        const {viewRatio, offset} = computeViewPortRatio( lmWidth, lmHeight, size.width, size.height );
        onViewPortChange({zoom:1, x:offset.x, y:offset.y}, true);
        return {viewRatio, offset};
    },[lmWidth, lmHeight, size, onViewPortChange]);

    const handleDragEnd = useCallback(({target})=>{
        onViewPortChange({...viewPort, x: target.x(), y:target.y()});
    },[viewPort, onViewPortChange]);

    const dragBoundaries = useCallback(({x, y})=>{
        const scale = viewPort.zoom < 1? 1 : viewPort.zoom;
        const minX = -size.width*scale + size.width + offset.x;
        const minY = -size.height*scale  + size.height + offset.y;
        const maxX =offset.x;
        const maxY =offset.y;

        return {
            x: x<minX? minX : x>maxX?maxX:x,
            y: y<minY? minY : y>maxY?maxY:y,
        };
    },[size, viewPort, offset]);

    //------------  In map buttons
    const centerHome=useCallback((e)=>{
        e.cancelBubble=true;
        onViewPortChange({zoom:1, x:offset.x, y:offset.y});
    },[onViewPortChange, offset.x, offset.y]);

    const moreZoom = useCallback((e)=>{
        e.cancelBubble=true;
        onViewPortChange( {...viewPort, zoom: maxDecimals(viewPort.zoom + 0.2,2)} );
    },[viewPort, onViewPortChange]);

    const lessZoom = useCallback((e)=>{
        e.cancelBubble=true;
        if(viewPort.zoom <= 1) return;
        onViewPortChange( {...viewPort, zoom: maxDecimals(viewPort.zoom - 0.2,2)} );
    },[viewPort, onViewPortChange]);

    // Handle stage click taking zoom and drag into account
    const handleStageClick = useCallback((e)=>{
        e.stagePosition = getRelativePointerPosition(interactiveLayerRef.current);
        onStageClick && onStageClick(e);
    },[onStageClick]);

    //------  Bottom name in a square ----
    const text = (()=>{
        if(!selectedItem && !selectedVisit)
            return null;
        if(selectedVisit)
            return guestFullName(selectedVisit.guest);
        if(selectedItem && selectedItem.type === 'food_bar')
            return selectedItem.name;
        return  null;
    }) ();

    const isItemReserved = (item)=>{
        if(!reservations)
            return false;
        return !!_.find(reservations, reservation=>_.find(reservation.seats, s=>s.id===item.id));
    }

    return (
        <div className={classNames('LoungeMap', className)} ref={containerRef}>
            <Stage
                width={size.width}
                height={size.height}
                onClick={handleStageClick}
                ref={stageRef}
            >
                <Layer
                    scaleX={viewPort.zoom*viewRatio}
                    scaleY={viewPort.zoom*viewRatio}
                    draggable
                    x={viewPort.x}
                    y={viewPort.y}
                    dragBoundFunc={dragBoundaries}
                    onDragEnd={handleDragEnd}
                    ref={interactiveLayerRef}
                >
                    <Rect
                        x={0}
                        y={0}
                        width={size.width/viewRatio}
                        height={size.height/viewRatio}
                    />
                    {editable &&
                    <Rect
                        x={0}
                        y={0}
                        width={lmWidth}
                        height={lmHeight}
                        stroke={'red'}
                        strokeWidth={4}
                    />}
                    {loungeMap && loungeMap.loungeMapItems.map( item=>
                        <KCMapItem
                            mapType={mapType}
                            loungeMapItem={item}
                            editable={editable}
                            onChange={onItemChange}
                            onClick={onItemSelect}
                            selected={selectedItem?.id===item.id || isSeatInVisit(item, selectedVisit)}
                            reserved={isItemReserved(item)}
                            extraInfo={extraInfo}
                            key={item.id}
                        />
                    )}
                </Layer>
                <Layer>
                    {text && !hideText && <>
                        <Rect
                            fill={'white'}
                            cornerRadius={5}
                            height={50}
                            width={600}
                            y={size.height-50}
                            x={size.width/2-300}
                        />
                        <Text
                            height={50}
                            width={600}
                            y={size.height-50}
                            x={size.width/2-300}
                            verticalAlign={'middle'}
                            align={'center'}
                            fontSize={28}
                            text={text}
                            fill={'black'}
                            fontFamily={'Montserrat-medium, Roboto'}
                        />
                    </>}
                    <KCPathButton
                        width={60}
                        height={50}
                        x={10}
                        y={size.height-55}
                        onClick={centerHome}
                        pathScale={0.08}
                        path={kcPaths.home}
                        />
                    <KCPathButton
                        width={60}
                        height={50}
                        x={76}
                        y={size.height-50}
                        onClick={moreZoom}
                        pathScale={0.06}
                        path={kcPaths.zoomPlus}
                        />
                    <KCPathButton
                        width={60}
                        height={50}
                        x={125}
                        y={size.height-50}
                        onClick={lessZoom}
                        pathScale={0.06}
                        path={kcPaths.zoomMinus}
                        pathFill={viewPort.zoom <= 1? 'gray':'white'}
                        />
                </Layer>
            </Stage>
        </div>
    );
};

export default LoungeMap;
