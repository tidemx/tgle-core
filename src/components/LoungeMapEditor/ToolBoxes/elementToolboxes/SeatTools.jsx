import React, {useCallback} from 'react';

const SeatTools = ({loungeMapItem, onChange})=>{

    // ---- Config change handler ----
    const handleTypeChange = useCallback((e)=>{
        onChange({...loungeMapItem, seatType: e.target.value});
    },[loungeMapItem, onChange]);

    return (<>
        <span className='toolbox-label'>Tipo:</span>{' '}
        <select className={'toolbox-input'} value={loungeMapItem.seatType} onChange={handleTypeChange}>
            <option value='normal'>Normal</option>
            <option value='highStool'>Banco alto</option>
            <option value='zeroGravity'>Gravedad cero</option>
        </select><br/>
    </>);
};

export default SeatTools;
