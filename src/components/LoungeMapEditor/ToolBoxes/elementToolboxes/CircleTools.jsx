import React from 'react';
import InputConfigTool from "./InputConfigTool";

const CircleTools = ({loungeMapItem, onChange})=>{

    return (<>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='width' displayName='Ancho' type='number'/>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='height' displayName='Alto' type='number'/>
    </>);
};

export default CircleTools;
