import React, {useMemo, useCallback} from 'react';
import InputConfigTool from "./InputConfigTool";

const MAX_FONT_SIZE = 60;
const MIN_FONT_SIZE = 20;

const TextTools = ({loungeMapItem, onChange})=>{

    // ---- Config change handler ----
    const handleConfigChange = useCallback((field, type, e)=>{
        let value = e.target.value;
        if(type === 'number')
            value = Number(value)||0;
        onChange({...loungeMapItem, config: {...loungeMapItem.config, [field]:value} });
    },[loungeMapItem, onChange]);

    const fontOptions = useMemo(()=>{
        const options = [];
        for( let i=MIN_FONT_SIZE; i<=MAX_FONT_SIZE; i+=2)
            options.push(<option value={i} key={i}>{i}</option>);
        return options;
    },[]);

    return (<>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='rotation' displayName='Rotación' type='number'/>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='scaleX' displayName='Escala x' type='number'/>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='scaleY' displayName='Escala y' type='number'/>
        <span className='toolbox-label'>Tamaño:</span>{' '}
        <select className={'toolbox-input'} value={loungeMapItem.config.fontSize} onChange={e=>handleConfigChange('fontSize', 'number', e)}>
            {fontOptions}
        </select><br/>
        <span className='toolbox-label'>Alinear:</span>{' '}
        <select className={'toolbox-input'} value={loungeMapItem.config.align||''} onChange={e=>handleConfigChange('align', 'text', e)}>
            <option value='left'>Izquierda</option>
            <option value='center'>Centro</option>
            <option value='right'>Derecha</option>
        </select><br/>
        <span className='toolbox-label'>Texto:</span>
        <textarea
            className='toolbox-input full-width'
            style={{textAlign: loungeMapItem.config.align||'left'}}
            value={loungeMapItem.config.text}
            onChange={e=>handleConfigChange('text', 'text', e)}
        /> <br/>
    </>);
};

export default TextTools;
