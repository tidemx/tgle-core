import React from 'react';
import InputConfigTool from "./InputConfigTool";

const FoodBarTools = ({loungeMapItem, onChange})=>{

    return (<>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='rotation' displayName='Rotación' type='number'/>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='width' displayName='Ancho' type='number'/>
        <InputConfigTool loungeMapItem={loungeMapItem} onChange={onChange} field='height' displayName='Alto' type='number'/>
    </>);
};

export default FoodBarTools;
