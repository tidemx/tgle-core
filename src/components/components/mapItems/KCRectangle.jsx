import React from 'react';
import {Rect} from 'react-konva';
import KCNativeShape from "./KCNativeShape";
import {positiveSizeTransformEnd} from "../../../utils/konvaUtils";

const KCRectangle = (props)=>{

    return <KCNativeShape {...props} Shape={Rect} onTransformEnd={positiveSizeTransformEnd}/>
};

export default KCRectangle;
