import React from 'react';
import {Circle} from 'react-konva';
import KCNativeShape from "./KCNativeShape";

const transformerProps = {
    enabledAnchors: ['top-left', 'top-right', 'bottom-left', 'bottom-right'],
    rotateEnabled:false,
};
const KCCircle = (props)=>{

    return <KCNativeShape {...props} Shape={Circle} transformerProps={transformerProps}/>
};

export default KCCircle;
