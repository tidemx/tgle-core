import React, {useCallback, useLayoutEffect, useRef} from 'react';
import {Transformer} from 'react-konva';
import {defaultRotationSnaps, defaultTransformEnd} from "../../../utils/konvaUtils";
import {defaultShapeColor} from "../../../utils/loungeMapItemUtils";

const KCNativeShape = ({
                           editable,
                           loungeMapItem,
                           onChange,
                           onTransformEnd=defaultTransformEnd,
                           onClick,
                           selected,
                           Shape,
                           transformerProps={},
                            ...restProps
})=>{

    const componentRef = useRef(null);
    const transformerRef = useRef(null);

    const handleDragEnd = useCallback((e)=>{
        e.cancelBubble=true;
        const position = e.currentTarget.getPosition();
        onChange({
            ...loungeMapItem,
            config:{
                ...loungeMapItem.config,
                x: Math.round(position.x),
                y: Math.round(position.y),
            }
        });
    }, [loungeMapItem, onChange]);

    const handleTransformEnd = useCallback(
        (e)=>onTransformEnd(e, loungeMapItem, onChange),
        [loungeMapItem, onChange, onTransformEnd]
    );

    useLayoutEffect(()=>{
        if(selected) {
            transformerRef.current.setNode(componentRef.current);
            transformerRef.current.getLayer().batchDraw();
        }
    },[selected]);

    const handleClick = useCallback((e)=>{
        e.cancelBubble=true;
        onClick(loungeMapItem);
    },[loungeMapItem, onClick]);

    const { ...config}= loungeMapItem.config;

    return (
        <>
            <Shape
                {...config}
                fill={selected?'green':(loungeMapItem.config.fill||defaultShapeColor)}
                draggable={editable}
                onDragEnd={handleDragEnd}
                onClick={handleClick}
                onTap={handleClick}
                onTransformEnd={handleTransformEnd}
                ref={componentRef}
                {...restProps}
            />
            {selected && <Transformer
                ref={transformerRef}
                rotationSnaps={defaultRotationSnaps}
                {...transformerProps}
            />}
        </>
    );
};

export default KCNativeShape;
