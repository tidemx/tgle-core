import React, {useCallback, useMemo} from 'react';
import KCSeat, {SELECTED_COLOR, DISABLED_COLOR} from './KCSeat';
import {defaultDragEnd} from "../../../utils/konvaUtils";
import {Group} from 'react-konva';

const KCSeatEditable = ({selected, loungeMapItem: item, onClick, onChange})=>{
    const color = useMemo(()=>selected?SELECTED_COLOR:DISABLED_COLOR, [selected]);
    const handleClick = useCallback((e)=>{
        e.cancelBubble=true;
        onClick(item);
    },[item, onClick]);
    const handleDragEnd = useCallback(
        e=>{
            defaultDragEnd(e, item, onChange);
        }, [item, onChange]
    );


    return (
        <Group
            draggable
            onDragEnd={handleDragEnd}
            {...item.config}
        >
            <KCSeat
                color={color}
                handleClick={handleClick}
            />
        </Group>
    );
};

export default KCSeatEditable;
