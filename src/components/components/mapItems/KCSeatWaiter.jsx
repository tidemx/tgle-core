import React, {/*useEffect, */useLayoutEffect, useState, useMemo, useRef, useCallback} from 'react';
import {isAFreeSeat, seatBelongsToWaiter} from '../../../utils/loungeMapItemUtils';
import {visitHasPeopleStanding} from '../../../utils/loungeMapUtils';
import KCSeat, {SELECTED_COLOR, DISABLED_COLOR} from './KCSeat';
import {getNextFlightExpirationBarPercentage} from "../../../utils/modelUtils/flightsUtil";
import {getCheckInsExpirationBarPercentage} from "../../../utils/modelUtils/checkInUtils";
import {Circle, Group} from 'react-konva';
import _ from 'lodash';

const DESCOMPUESTO_RED = '#b33325';
const NOT_SO_GOOD_COLOR = 'orange';
const OK_GREEN = '#8ecd20';
const OCCUPIED_YELLOW = '#fffb00';
const RESERVED_COLOR = '#be00ff';


const getColor = ({selected, item, timeLeft, reserved})=>{
    if(selected)
        return SELECTED_COLOR;
    if(!item.visit && reserved)
        return RESERVED_COLOR;
    if(!item.visit)
        return DISABLED_COLOR;
    if( timeLeft > 0.5)
        return OK_GREEN;
    if( timeLeft > 0.01)
        return NOT_SO_GOOD_COLOR;
    return DESCOMPUESTO_RED;
};

const disabledState = {
    color: DISABLED_COLOR,
    arc: 1,
    markAnimation: false,
    highlightAnimation: false
};

const useSeatAnimations = ({forcedState, highlight, showMarkAnimation}) => {
    const forcedMark = forcedState?forcedState.markAnimation:undefined;
    const forcedHighlight = forcedState?forcedState.highlightAnimation:undefined;
    const animatedCircleRef = useRef(null);
    const dashedCircleRef = useRef(null);

    // Ripple animation
    useLayoutEffect(()=>{
        if(!animatedCircleRef.current) return ()=>{};//....
        if(forcedHighlight!==undefined){
            if(forcedHighlight===false){
                return ()=>{};
            }
        }
        if(!highlight&&forcedHighlight!==true) return ()=>{};

        const duration = 1;
        const animation = ()=> {
            animatedCircleRef.current.setAttr('radius', 20);
            animatedCircleRef.current.setAttr('opacity', 1);
            animatedCircleRef.current.to({
                radius: 90,
                opacity: 0,
                duration: 1
            });
        };
        animation();
        const interval = setInterval(animation, duration*1000);

        return ()=>clearInterval(interval);

    },[highlight, forcedHighlight]);

    // Rotating mark animation
    useLayoutEffect(()=>{

        if(forcedMark!==undefined){
            if(forcedMark===false){
                return ()=>{};
            }
        }
        if(!showMarkAnimation&&forcedMark!==true) return ()=>{};

        const duration = 6;
        const animation = ()=> {
            dashedCircleRef.current.setAttr('rotation', 0);
            dashedCircleRef.current.to({
                rotation: 360,
                duration
            });
        };
        animation();
        const interval = setInterval(animation, duration*1000);

        return ()=>clearInterval(interval);
    },[showMarkAnimation, forcedMark]);
    return [animatedCircleRef, dashedCircleRef];
};

const KCSeatWaiter = ({loungeMapItem: item, selected, onChange, onClick, extraInfo={}, reserved})=>{
    const groupRef = useRef(null);
    const {visit, waiter, unsetNearestSeat, setNearestSeat, nearestCandidate, setForceState} = extraInfo;
    const [coords, setCoords] = useState(_.pick(item.config, ['x', 'y']));
    const [dragging, setDragging] = useState(false);

    const timeForFlight = item.visit? getNextFlightExpirationBarPercentage(item.visit.flight):1;
    const timeForCheckOut = item.visit? getCheckInsExpirationBarPercentage(item.visit.checkIns):1;
    const timeLeft = _.min([timeForFlight, timeForCheckOut]);

    const highlightFreeSeats = visit && visitHasPeopleStanding(visit);
    const highlight=highlightFreeSeats && isAFreeSeat(item);
    const belongsToWaiter=seatBelongsToWaiter(waiter, item);

    const showMarkAnimation = belongsToWaiter;

    const color = useMemo(()=>getColor({
        selected, item, timeLeft, nearestCandidate, reserved
    }), [selected, item, timeLeft, nearestCandidate, reserved]);

    const calculatedState = useMemo(()=>({
        arc: timeLeft,
        color,
        markAnimation: showMarkAnimation,
        showHighlightAnimation: highlight
    }), [timeLeft, color, showMarkAnimation, highlight]);

    const forcedState = item.force?(item.force==='disabled'?disabledState:item.force):null;

    const handleDragEnd = useCallback(
        e=>{
            setDragging(false);
            const dragEndCoords = {x: e.target.x(), y: e.target.y()};
            unsetNearestSeat();

            setForceState(calculatedState);
            e.cancelBubble = true;

            onChange(dragEndCoords, item);

            setCoords({x:0, y:0});
            setCoords(_.pick(item.config, ['x', 'y']));
        },
        [item, onChange, setForceState, unsetNearestSeat, calculatedState]);
    const handleDragMove = useCallback((e)=>{
        setDragging(true);
        setNearestSeat({
            x: e.target.x(),
            y: e.target.y()
        }, item);
    }, [setNearestSeat, item]);

    const occupiedSeat = !!item.visit;

    const handleClick = useCallback((e)=>{
        e.cancelBubble=true;
        onClick(item);
    },[item, onClick]);

    const [animatedCircleRef, dashedCircleRef] = useSeatAnimations({highlight, showMarkAnimation, forcedState});
    const maybeYellow = (nearestCandidate?.id === item.id)?
          OCCUPIED_YELLOW:color;

    return (
        <>
            {occupiedSeat&& (
             <>
                 {/*Any occupied seat can be dragged for reassigning it. We'll duplicate a gray seat below the one dragged.*/}
                <KCSeat
                    color={nearestCandidate?.id===item.id?OCCUPIED_YELLOW:disabledState.color}
                    arc={dragging?disabledState.arc:timeLeft}
                    {...item.config}
                />
             </>
            )}
            <Group
                draggable={occupiedSeat}
                onDragMove={handleDragMove}
                onDragEnd={handleDragEnd}
                {...item.config}
                {...coords}
                ref={groupRef}
            >
                 {/*Also when a seat belongs to the selected waiter, we'll show an animation of a rotating target around the seat*/}
        {(forcedState?forcedState.markAnimation:showMarkAnimation)&&
                    <Circle
                        radius={32}
                        stroke={'#FF8902'}
                        strokeWidth={4}
                        dash={[16]}
                        ref={dashedCircleRef}
                    />
                }
                {/*And when a selected visit has people standing, and the current seat is free, we'll show a ripple animation*/}
        {(forcedState?forcedState.highlightAnimation:highlight) &&
                <Circle
                    radius={20}
                    stroke={'#66ff24'}
                    strokeWidth={4}
                    ref={animatedCircleRef}
                    listening={false}
                />}

            <KCSeat
        color={forcedState?forcedState.color:(dragging?color:maybeYellow)}
        arc={forcedState?forcedState.arc:timeLeft}
        handleClick={handleClick}
            />
            </Group>
        </>
    );
};

export default KCSeatWaiter;
