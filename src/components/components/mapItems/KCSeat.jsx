import React from 'react';
import {Circle, Arc} from "react-konva";

export const SELECTED_COLOR = '#3D79ff';
export const DISABLED_COLOR = 'gray';


const KCSeat = ({color, arc=1, handleClick, ...extraProps})=>{
    // arc is a number in [0, 1]
    return (
        <>
            <Arc
                innerRadius={25}
                outerRadius={28}
                fill={color}
                angle={arc*360}
                onClick={handleClick}
                onTap={handleClick}
                {...extraProps}
             />
            <Circle
                radius={20}
                fill={color}
                onClick={handleClick}
                onTap={handleClick}
                {...extraProps}
            />

        </>
    );
};

export default KCSeat;
