import React, {useCallback, useLayoutEffect, useRef} from 'react';
import {Group, Rect, Transformer} from "react-konva";
import {defaultDragEnd, defaultRotationSnaps, defaultTransformEnd} from "../../../utils/konvaUtils";

export const SELECTED_COLOR = '#3D79ff';
export const DISABLED_COLOR = 'lightgray';


const defaultConfig = {cornerRadius: 10};

const KCFoodBar = ({
                           editable,
                           loungeMapItem,
                           onChange,
                           onClick,
                           selected,
                       })=>{

    const componentRef = useRef(null);
    const transformerRef = useRef(null);

    useLayoutEffect(()=>{
        if(selected && editable) {
            transformerRef.current.setNode(componentRef.current);
            transformerRef.current.getLayer().batchDraw();
        }
    },[selected, editable]);

    const handleDragEnd = useCallback(
        e=>defaultDragEnd(e, loungeMapItem, onChange),
        [loungeMapItem, onChange]);

    const handleTransformEnd = useCallback(
        (e)=>defaultTransformEnd(e, loungeMapItem, onChange),
        [loungeMapItem, onChange]
    );

    const handleClick = useCallback((e)=>{
        e.cancelBubble=true;
        onClick(loungeMapItem);
    },[loungeMapItem, onClick]);

    const {x, y, ...config} = loungeMapItem.config;

    const fill =
        editable && selected? 'green':
        !editable && selected? SELECTED_COLOR:
        DISABLED_COLOR;

    return (
        <>
            <Group
                draggable={editable}
                onDragEnd={handleDragEnd}
                x={x}
                y={y}
            >
                <Rect
                    {...config}
                    {...defaultConfig}
                    onTransformEnd={handleTransformEnd}
                    ref={componentRef}
                    x={0}
                    y={0}
                    fill={fill}
                    onClick={handleClick}
                    onTap={handleClick}
                />

                {selected && editable && <Transformer
                    ref={transformerRef}
                    rotationSnaps={defaultRotationSnaps}
                />}
            </Group>
        </>
    );
};

export default KCFoodBar;
