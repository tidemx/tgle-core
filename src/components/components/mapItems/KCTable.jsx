import React from 'react';
import {Rect} from "react-konva";
import KCNativeShape from "./KCNativeShape";
import {positiveSizeTransformEnd} from "../../../utils/konvaUtils";


const KCTable = (props)=>{
    return <KCNativeShape
        {...props}
        Shape={Rect}
        onTransformEnd={positiveSizeTransformEnd}
        fill={'#0d2163'}
        stroke={'#3D79ff'}
    />
};

export default KCTable;
