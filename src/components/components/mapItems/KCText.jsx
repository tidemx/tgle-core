import React from 'react';
import {Text} from 'react-konva';
import KCNativeShape from "./KCNativeShape";


const textTransformEnd = (event, loungeMapItem, onChange)=> {
    const node = event.currentTarget;
    const newItem = {
        ...loungeMapItem,
        config: {
            ...loungeMapItem.config,
            //TODO: Take rotation into consideration when scaling to < 0
            x: node.scaleX() > 0 ? node.x() : node.x() - node.width() * Math.abs(node.scaleX()),
            y: node.scaleY() > 0 ? node.y() : node.y() - node.height() * Math.abs(node.scaleY()),
            scaleX: Math.abs(node.scaleX()),
            scaleY: Math.abs(node.scaleY()),
            rotation: node.rotation()
        }
    };
    onChange(newItem);
};


const KCText = (props)=>{

    return <KCNativeShape
        {...props}
        Shape={Text}
        onTransformEnd={textTransformEnd}
        fontFamily={'Montserrat-medium, Roboto'}
    />
};

export default KCText;
