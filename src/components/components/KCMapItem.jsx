import React from 'react';
import _ from "lodash";
import {mapItemsDefinition} from "../../utils/loungeMapItemUtils";


const KCMapItem = ({loungeMapItem, mapType, ...props})=>{

    const definition = _.find(mapItemsDefinition(mapType), def=>def.type===loungeMapItem.type);

    if(!definition) {
        console.warn('Got map item of type ' + loungeMapItem.type + ' but there\'s no definition for it.');
        return null;
    }

    const Component = definition.component;

    return <><Component
        loungeMapItem={loungeMapItem}
        listening={props.editable || definition.interactive}
        {...props}
    />
    </>;
};

export default KCMapItem;
