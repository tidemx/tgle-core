import React from 'react';
import {Path, Rect} from "react-konva";

const KCPathButton = ({x, y, width, height, pathScale, path, pathFill='white', onClick})=>{

    return <>
        <Rect
            width={width}
            height={height}
            x={x}
            y={y}
            onClick={onClick}
            onTap={onClick}
        />
        <Path
            data={path}
            fill={pathFill}
            scaleX={pathScale}
            scaleY={pathScale}
            x={x}
            y={y}
            listening={false}
        />
        </>
};

export default KCPathButton;
