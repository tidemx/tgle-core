export const initialViewPort = { zoom:1, x:0, y:0 };

export const getRelativePointerPosition = (node)=>{
    // the function will return pointer position relative to the passed node
    var transform = node.getAbsoluteTransform().copy();
    // to detect relative position we need to invert transform
    transform.invert();

    // get pointer (say mouse or touch) position
    var pos = node.getStage().getPointerPosition();

    // now we find relative point
    return transform.point(pos);
};


export const defaultRotationSnaps = [0,45,90,135,180,225,270,315];

export const defaultTransformEnd = (event, loungeMapItem, onChange)=> {
    const node = event.currentTarget;
    const newItem = {
        ...loungeMapItem,
        config: {
            ...loungeMapItem.config,
            width: node.width() * Math.abs(node.scaleX()),
            height: node.height() * Math.abs(node.scaleY()),
            rotation: node.rotation()
        }
    };
    node.scaleX(1);
    node.scaleY(1);
    onChange(newItem);
};

/**
 * This handles the transformation form scale to width and height. It handles negative scales by making them absolute
 * and moving the position of the element.
 * @param event KonvaEvent
 * @param loungeMapItem
 * @param onChange
 */
export const positiveSizeTransformEnd = (event, loungeMapItem, onChange)=> {
    const node = event.currentTarget;
    const newItem = {
        ...loungeMapItem,
        config: {
            ...loungeMapItem.config,
            //TODO: Take rotation into consideration when scaling to < 0
            x: node.scaleX() > 0 ? node.x() : node.x() - node.width() * Math.abs(node.scaleX()),
            y: node.scaleY() > 0 ? node.y() : node.y() - node.height() * Math.abs(node.scaleY()),
            width: node.width() * Math.abs(node.scaleX()),
            height: node.height() * Math.abs(node.scaleY()),
            rotation: Math.round(node.rotation())
        }
    };
    node.scaleX(1);
    node.scaleY(1);
    onChange(newItem);
};

export const defaultDragEnd = (event, loungeMapItem, onChange)=> {
    event.cancelBubble = true;
    const position = event.currentTarget.getPosition();
    return onChange({
        ...loungeMapItem,
        config: {
            ...loungeMapItem.config,
            x: Math.round(position.x),
            y: Math.round(position.y),
        }
    });
};

export const maxDecimals = ( num, decimals=10 )=>{
    const factor = Math.pow( 10,decimals );
    return Math.round(num * factor)/factor;
};
