import _ from 'lodash';
import {prepareLoungeMapItemForRender, prepareLoungeMapItemForServer} from "./loungeMapItemUtils";


export const computeViewPortRatio = (mapWidth, mapHeight, viewWidth, viewHeight)=> {
    const horizontalRatio = viewWidth / mapWidth;//  1200/1000 = 1.2
    const verticalRatio = viewHeight / mapHeight;//   800/1000 = 0.8

    let viewRatio;
    let offset;
    if (verticalRatio < horizontalRatio) {
        viewRatio = verticalRatio;
        offset = {
            x: (viewWidth-(mapWidth*viewRatio))/2,
            y: 0,
        };
    }
    else {
        viewRatio = horizontalRatio;
        offset = {
            x: 0,
            y: (viewHeight-(mapHeight*viewRatio))/2,
        };
    }

    return {viewRatio, offset};
};

export const prepareLoungeMapForServer = ( loungeMap )=>{

    const prepared = {...loungeMap};
    prepared.loungeMapItems = prepared.loungeMapItems.map( prepareLoungeMapItemForServer );
    delete prepared.lounge;

    return prepared;
};

export const prepareLoungeMapForRender = (loungeMap)=>{

    const prepared = {...loungeMap};
    prepared.loungeMapItems = loungeMap.loungeMapItems.map(prepareLoungeMapItemForRender);
    return prepared;
};

export const addVisitsToLoungeMap = (visits, loungeMap)=>{

    const newMap = {...loungeMap};
    const seatIdAsKeyVisitAsValue = {};
    _.forEach(visits,
            visit=>
                _.forEach(visit.seats||[],
                        seat=>
                            seatIdAsKeyVisitAsValue[seat.id] = visit
                ));

    newMap.loungeMapItems = loungeMap.loungeMapItems.map( item=>{
        if(seatIdAsKeyVisitAsValue[item.id])
            return {...item, visit:seatIdAsKeyVisitAsValue[item.id] };
        return item;
    });
    return newMap;
};

export const canAssignVisitToSeat = (visit, seat)=>{
    return !!(
        visit &&
        seat &&
        seat.type === 'seat' &&
        !seat.visit &&
        visitHasPeopleStanding(visit)
    )
};

export const visitHasPeopleStanding = (visit)=> !!(!visit.seats|| visit.seats.length < visit.activePersons);

export const loungeMapListSGroups = [
    'lounge_map_read_id',
    'lounge_map_read_lounge',
        'lounge_read_id',
        'lounge_read_name',
];

export const loungeMapEditorSGroups = [
    'lounge_map_read_id',
    'lounge_map_read_config',
    'lounge_map_read_lounge',
        'lounge_read_id',
        'lounge_read_name',
    'lounge_map_read_lounge_map_items',
        'lounge_map_item_read',
];

export const loungeMapDetailsSGroups = [
    'lounge_map_read_id',
    'lounge_map_read_config',
    'lounge_map_read_lounge',
        'lounge_read_id',
        'lounge_read_name',
    'lounge_map_read_lounge_map_items',
        'lounge_map_item_read',
        'seat_read',
            'seat_read_visit',
                'visit_read_id',
                'visit_read_active_persons',
                'visit_read_guest',
                    'guest_read_id',
                    'guest_read_name',
                    'guest_read_pat_last_name',
                'visit_read_flight',
                    'flight_read',
                'visit_read_check_ins',
                    'check_in_read_id',
                    'check_in_read_expiration_date',
];

// Methods from other files that are copied to be shared between projects

export const guestFullName=guest=>guest?`${guest.name||''} ${guest.patLastName||''} ${guest.matLastName||''}`:'';
