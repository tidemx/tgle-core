
export const getProperty = (obj, ...args)=>{
    return args.reduce((obj, level) => obj && obj[level], obj)
};
// i think.. this could be replaced with the optional chaining operator...
