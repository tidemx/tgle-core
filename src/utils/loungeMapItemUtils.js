import KCRectangle from "../components/components/mapItems/KCRectangle";
import KCCircle from "../components/components/mapItems/KCCircle";
import KCText from "../components/components/mapItems/KCText";
import _ from "lodash";
import TextTools from "../components/LoungeMapEditor/ToolBoxes/elementToolboxes/TextTools";
import RectTools from "../components/LoungeMapEditor/ToolBoxes/elementToolboxes/RectTools";
import CircleTools from "../components/LoungeMapEditor/ToolBoxes/elementToolboxes/CircleTools";
import KCSeatEditable from "../components/components/mapItems/KCSeatEditable";
import KCSeatWaiter from "../components/components/mapItems/KCSeatWaiter";
import SeatTools from "../components/LoungeMapEditor/ToolBoxes/elementToolboxes/SeatTools";
import KCTable from "../components/components/mapItems/KCTable";
import TableTools from "../components/LoungeMapEditor/ToolBoxes/elementToolboxes/TableTools";
import KCFoodBar from "../components/components/mapItems/KCFoodBar";
import FoodBarTools from "../components/LoungeMapEditor/ToolBoxes/elementToolboxes/FoodBarTools";

export const defaultShapeColor = '#2b2d65';

export const prepareLoungeMapItemForServer = ( loungeMapItem )=>{
    const prepared = {...loungeMapItem};

    if(!prepared.config)
        prepared.config = {};

    prepared.config.type = prepared.type;
    prepared.type = mapTypesToServerTypes(prepared.type);

    if(prepared.isNew) {
        delete prepared.id;
        delete prepared.isNew;
    }

    return prepared;
};

export const prepareLoungeMapItemForRender = (loungeMapItem)=>{
    const prepared = {...loungeMapItem};
    if(prepared.config && prepared.config.type)
        prepared.type = prepared.config.type;
    return prepared;
};

/**
 * We have more types in the client than in the server. Unknown types for the server are saved as "generic" and the client
 * type is saved in the config object.
 *
 * @type {string[]}
 */
const availableServerTypes = ['generic', 'food_bar', 'table', 'seat', 'zone'];
export const mapTypesToServerTypes = ( type )=>{
    if( availableServerTypes.indexOf(type)!==-1)
        return type;
    return 'generic';
};

const commonMapItems = [
    {
        type:'rect',
        name:'Rectángulo',
        component:KCRectangle,
        toolbox: RectTools,
        interactive: false,
        defaultConfig: {width: 50, height: 50, fill: defaultShapeColor, cornerRadius:8}
    },
    {
        type:'circle',
        name:'Círculo',
        component:KCCircle,
        toolbox: CircleTools,
        interactive: false,
        defaultConfig: {width: 50, height: 50, fill: defaultShapeColor}
    },
    {
        type:'text',
        name:'Texto',
        component: KCText,
        toolbox: TextTools,
        interactive: false,
        defaultConfig: {fill: 'white', text: 'Escribe un texto...', fontSize: 30, fontFamily: 'Roboto', scaleX:1, scaleY: 1}
    },
    {
        type:'table',
        name:'Mesa',
        component: KCTable,
        toolbox: TableTools,
        defaultConfig: {fill: 'gray', width: 50, height: 80, cornerRadius: 10}
    },
    {
        type:'food_bar',
        name:'Barra de comida',
        component: KCFoodBar,
        toolbox: FoodBarTools,
        defaultConfig: {width: 100, height: 200}
    },
];

const gleFrontendItems = [
    ...commonMapItems,
    {
        type:'seat',
        name:'Asiento',
        component: KCSeatEditable,
        objectPrototype:{ seatType:'normal' },
        toolbox: SeatTools
    }
];
const davinciItems = [
    ...commonMapItems,
    {
        type:'seat',
        name:'Asiento',
        component: KCSeatWaiter,
        objectPrototype:{ seatType:'normal' },
        toolbox: SeatTools
    }
];

export const mapItemsDefinition = mapType=>{
    if(mapType==='default'||mapType==='gle-frontend-map'){
        return gleFrontendItems;
    }
    return davinciItems;
};

export const createMapItem = (itemType, position, loungeMapItems, mapType='default')=>{

    const definition = _.find(mapItemsDefinition(mapType), def=>def.type===itemType);

    return {
        id: Math.random(),
        name: getNextItemName(definition, loungeMapItems),
        isNew: true,
        type: itemType,
        config: {...position, ...(definition.defaultConfig||{})},
        ...(definition.objectPrototype||{})
    }
};

const getNextItemName = (itemDefinition, loungeMapItems)=>{
    const templateRegexp = new RegExp(`^${itemDefinition.name} ([0-9]+)$`);
    templateRegexp.exec = templateRegexp.exec.bind(templateRegexp);
    const lastName = _.chain(loungeMapItems).map('name').filter().map(templateRegexp.exec).filter().sortBy('1').last().value();

    if(lastName)
        return `${itemDefinition.name} ${Number(lastName[1])+1}`;
    else
        return `${itemDefinition.name} 1`;
};

export const isSeatInVisit = (seat, visit)=>{
    if(!visit || !seat || !visit.seats || !visit.seats.length || seat.type!=='seat')
        return false;
    return !!_.find(visit.seats, s=>s.id===seat.id);
};

export const isAFreeSeat = (loungeMapItem)=>{
    return !!(loungeMapItem && loungeMapItem.type === 'seat' && !loungeMapItem.visit);
};

export const seatBelongsToWaiter = (waiterId, seat)=>{
    return !!(
        seat &&
        seat.type === 'seat' &&
        seat.visit &&
        seat.visit.employeesAttendingVisit &&
        seat.visit.employeesAttendingVisit.length &&
        seat.visit.employeesAttendingVisit[0].employee &&
        seat.visit.employeesAttendingVisit[0].employee.id === waiterId
    );
};
