# Development
To develop changes on this repo, run yarn link on the app you're testing (davinci in this example):

`cd /WHERE/PROJECTS/ARE/tgle-core`
`yarn link`
`cd /WHERE/PROJECTS/ARE/davinci` 
`yarn link tgle-core`

Then run yarn start to run webpack dev server with reload enabled.

Probably a warning on having multiple copies of react may arise, solve it
as proposed on react docs

https://reactjs.org/warnings/invalid-hook-call-warning.html

running `npm link /WHERE/PROJECTS/ARE/davinci/node_modules/react` from this package