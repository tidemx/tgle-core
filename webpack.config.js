const {resolve, join} = require('path');

const components = [
    'LoungeMap',
];
const utils = [
    'currency/currencyUtils',
    'modelUtils/checkInUtils',
    'modelUtils/flightsUtil',
    'modelUtils/checkInUtils',
    'modelUtils/loungeAccessMethodsUtils',
    'modelUtils/paymentUtils',
    'formUtils',
    'jsUtils',
    'konvaUtils',
    'loungeMapItemUtils',
    'loungeMapUtils'
];

const toKeyVal = vs=>vs.reduce((obj, [key, val])=>({...obj, [key]: val}), {});
const componentsEntry = toKeyVal(components.map(
    c=>[join('components',c,'index'), './'+join('src/components',c,c+'.jsx')]));
const utilsEntry = toKeyVal(utils.map(
    u=>[join('utils', u, 'index'), './'+join('src/utils', u+'.js' )]));
const entry = {...componentsEntry, ...utilsEntry};

const commonConfig = {
    entry,
    output: {
        path: resolve('.'),
        filename: '[name].js',
        libraryTarget: 'commonjs2'
    },
    module: {
        rules: [
            {test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader!eslint-loader'},
            {test: /\.jsx$/, exclude: /node_modules/, loader: 'babel-loader!eslint-loader'},
            {test: /\.css$/, loader: 'style-loader!css-loader'},
            {test: /\.svg$/, loader: 'svg-inline-loader'},
            {test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader'},
        ]
    },
    externals: {
        react: {
            root: 'React',
            commonjs2: 'react',
            commonjs2: 'react',
            amd: 'react',
            umd: 'react'
        },
        'react-dom': {
            root: 'ReactDOM',
            commonjs2: 'react-dom',
            commonjs2: 'react-dom',
            amd: 'react-dom',
            umd: 'react-dom'
        }
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};

const productionConfig = {
    mode: 'production',
    ...commonConfig
};

const developmentConfig = {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        stats: {
            children: false,
            maxModules: 0
        },
        port: 3002,
        hot: true
    },
    ...commonConfig
};

module.exports = (env, argv)=>{
    if(argv.mode==='development') return developmentConfig;
    return productionConfig;
};
